package model

import (
	"fmt"
	"os"

	"github.com/jmoiron/sqlx"
)

func getenv(varName string, defaultValue string) string {
	v := os.Getenv(varName)
	if v == "" {
		return defaultValue
	}
	return v
}

func InitDB() (*sqlx.DB, error) {
	user := getenv("MARIADB_USERNAME", "root")
	pass := getenv("MARIADB_PASSWORD", "password")
	host := getenv("MARIADB_HOSTNAME", "localhost")
	dbname := getenv("MARIADB_DATABASE", "example")
	db, err := sqlx.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:3306)/%s", user, pass, host, dbname)+"?parseTime=True&loc=Asia%2FTokyo&charset=utf8mb4")
	if err != nil {
		return nil, err
	}
	return db, err
}
